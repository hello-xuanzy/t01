create database if not exists online_edu_da;
use online_edu_da;
drop table if exists time_school_signup_cnt;
create table time_school_signup_cnt(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    itcast_school_id      string,
    itcast_school_name    string,
    signup_cnt            bigint
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_school_signup_cnt;
create table time_type_school_signup_cnt(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    itcast_school_id      string,
    itcast_school_name    string,
    signup_cnt            bigint
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_subject_signup_cnt;
create table time_type_subject_signup_cnt(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    itcast_subject_id     string,
    itcast_subject_name   string,
    signup_cnt            bigint
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_school_subject_signup_cnt;
create table time_type_school_subject_signup_cnt(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    itcast_school_id      string,
    itcast_school_name    string,
    itcast_subject_id     string,
    itcast_subject_name   string,
    signup_cnt            bigint
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_channel_signup_cnt;
create table time_type_channel_signup_cnt(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    origin_channel        string,
    signup_cnt            bigint
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_tdepart_signup_cnt;
create table time_type_tdepart_signup_cnt(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    tdepart_id            int,
    tdepart_name          string,
    signup_cnt            bigint
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_relationship_rate;
create table time_type_relationship_rate(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    relationship_rate     string
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

drop table if exists time_type_appeal_rate;
create table time_type_appeal_rate(
    time_type             string,
    `year`                string,
    `month`               string,
    `day`                 string,
    origin_type           string,
    appeal_rate           string
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');

insert into online_edu_da.attendance_count
select class_id,
       date_day,
       morning_count,
       afternoon_count,
       evening_count
from online_edu_dws.dws_student_attendance;

insert into online_edu_da.attendance_probability
select class_id,
       date_day,
       morning_probability,
       afternoon_probability,
       evening_probability
from online_edu_dws.dws_student_attendance;

insert into online_edu_da.late_count
select class_id,
       date_day,
       morning_late_count,
       afternoon_late_count,
       evening_late_count
from online_edu_dws.dws_student_late;

insert into online_edu_da.late_probability
select class_id,
       date_day,
       morning_late_probability,
       afternoon_late_probability,
       evening_late_probability
from online_edu_dws.dws_student_late;

insert into online_edu_da.leave_count
select class_id,
       date_day,
       morning_leave_count,
       afternoon_leave_count,
       evening_leave_count
from online_edu_dws.dws_student_leave;

insert into online_edu_da.leave_probability
select class_id,
       date_day,
       morning_leave_probability,
       afternoon_leave_probability,
       evening_leave_probability
from online_edu_dws.dws_student_leave;

insert into online_edu_da.truancy_count
select class_id,
       date_day,
       morning_truancy_count,
       afternoon_truancy_count,
       evening_truancy_count
from online_edu_dws.dws_student_truancy;

insert into online_edu_da.truancy_probability
select class_id,
       date_day,
       morning_truancy_probability,
       afternoon_truancy_probability,
       evening_truancy_probability
from online_edu_dws.dws_student_truancy;
create table mysql.online_edu_da.attendance_count(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_count int comment '早上出勤人数',
    afternoon_count int comment '下午出勤人数',
    evening_count int comment '晚上出勤人数'
);

create table mysql.online_edu_da.attendance_probability(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_probability decimal(24,4) comment '早上出勤率',
    afternoon_probability decimal(24,4) comment '下午出勤率',
    evening_probability decimal(24,4) comment '晚上出勤率'
);


create table mysql.online_edu_da.late_count(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_late_count int comment '早上迟到人数',
    afternoon_late_count int comment '下午迟到人数',
    evening_late_count int comment '晚上迟到人数'
);


create table mysql.online_edu_da.late_probability(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_late_probability decimal(24,4) comment '早上迟到率',
    afternoon_late_probability decimal(24,4) comment '下午迟到率',
    evening_late_probability decimal(24,4) comment '晚上迟到率'
);


create table mysql.online_edu_da.leave_count(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_leave_count int comment '早上请假人数',
    afternoon_leave_count int comment '下午请假人数',
    evening_leave_count int comment '晚上请假人数'
);


create table mysql.online_edu_da.leave_probability(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_leave_probability decimal(24,4) comment '早上请假率',
    afternoon_leave_probability decimal(24,4) comment '下午请假率',
    evening_leave_probability decimal(24,4) comment '晚上请假率'
);


create table mysql.online_edu_da.truancy_count(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_truancy_count int comment '早上旷课人数',
    afternoon_truancy_count int comment '下午旷课人数',
    evening_truancy_count int comment '晚上旷课人数'
);


create table mysql.online_edu_da.truancy_probability(
    class_id int comment '班级id',
    date_day varchar comment '日期',
    morning_truancy_probability decimal(24,4) comment '早上旷课率',
    afternoon_truancy_probability decimal(24,4) comment '下午旷课率',
    evening_truancy_probability decimal(24,4) comment '晚上旷课率'
);





insert into mysql.online_edu_da.attendance_count
select * from hive.online_edu_da.attendance_count;

insert into mysql.online_edu_da.attendance_probability
select * from hive.online_edu_da.attendance_probability;

insert into mysql.online_edu_da.late_count
select * from hive.online_edu_da.leave_count;

insert into mysql.online_edu_da.late_probability
select * from hive.online_edu_da.late_probability;

insert into mysql.online_edu_da.leave_count
select * from hive.online_edu_da.leave_count;

insert into mysql.online_edu_da.leave_probability
select * from hive.online_edu_da.leave_probability;

insert into mysql.online_edu_da.truancy_count
select * from hive.online_edu_da.truancy_count;

insert into mysql.online_edu_da.truancy_probability
select * from hive.online_edu_da.truancy_probability;
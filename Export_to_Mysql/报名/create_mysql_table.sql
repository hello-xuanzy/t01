drop table if exists mysql.online_edu_da.time_school_signup_cnt;
create table mysql.online_edu_da.time_school_signup_cnt(
    time_type             varchar,
    "year"                varchar,
    "month"               varchar,
    "day"                 varchar,
    itcast_school_id      varchar,
    itcast_school_name    varchar,
    signup_cnt            bigint
);

drop table if exists mysql.online_edu_da.time_type_school_signup_cnt;
create table mysql.online_edu_da.time_type_school_signup_cnt(
    time_type             varchar,
    "year"                varchar,
    "month"               varchar,
    "day"                 varchar,
    origin_type           varchar,
    itcast_school_id      varchar,
    itcast_school_name    varchar,
    signup_cnt            bigint
);

drop table if exists mysql.online_edu_da.time_type_subject_signup_cnt;
create table mysql.online_edu_da.time_type_subject_signup_cnt(
    time_type             varchar,
    "year"                varchar,
    "month"               varchar,
    "day"                 varchar,
    origin_type           varchar,
    itcast_subject_id     varchar,
    itcast_subject_name   varchar,
    signup_cnt            bigint
);

drop table if exists mysql.online_edu_da.time_type_school_subject_signup_cnt;
create table mysql.online_edu_da.time_type_school_subject_signup_cnt(
    time_type             varchar,
    "year"                varchar,
    "month"               varchar,
    "day"                 varchar,
    origin_type           varchar,
    itcast_school_id      varchar,
    itcast_school_name    varchar,
    itcast_subject_id     varchar,
    itcast_subject_name   varchar,
    signup_cnt            bigint
);

drop table if exists mysql.online_edu_da.time_type_channel_signup_cnt;
create table mysql.online_edu_da.time_type_channel_signup_cnt(
    time_type            varchar,
    "year"               varchar,
    "month"              varchar,
    "day"                varchar,
    origin_type          varchar,
    origin_channel       varchar,
    signup_cnt           bigint
);

drop table if exists mysql.online_edu_da.time_type_tdepart_signup_cnt;
create table mysql.online_edu_da.time_type_tdepart_signup_cnt(
    time_type             varchar,
    "year"                varchar,
    "month"               varchar,
    "day"                 varchar,
    origin_type           varchar,
    tdepart_id            int,
    tdepart_name          varchar,
    signup_cnt            bigint
);

drop table if exists mysql.online_edu_da.time_type_relationship_rate;
create table mysql.online_edu_da.time_type_relationship_rate(
    time_type             varchar,
    "year"                varchar,
    "month"               varchar,
    "day"                 varchar,
    origin_type           varchar,
    relationship_rate     varchar
);

drop table if exists mysql.online_edu_da.time_type_appeal_rate;
create table mysql.online_edu_da.time_type_appeal_rate
(
    time_type   varchar,
    "year"      varchar,
    "month"     varchar,
    "day"       varchar,
    origin_type varchar,
    appeal_rate varchar
);
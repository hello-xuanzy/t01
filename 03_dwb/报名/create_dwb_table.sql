create database if not exists online_edu_dwb;
use online_edu_dwb;
drop table if exists dwb_signup_info_table;
create table dwb_signup_info_table(
    customer_id 				    int		     comment'学员id',
    area 			                string		 comment'地区',
    anticipat_signup_date           string       comment'下次回访时间',
    appeal_status 		            int		     comment'申诉状态',
    create_date_time                string       comment'创建日期',
    deleted                         int          comment'合法性判断的字段，如果为1，表示这一条意向信息已经被删除，不用参加统计',
    origin_type                     string       comment'线上线下',
    origin_channel                  string       comment'来源渠道',
    payment_state                   string       comment'支付状态',
    payment_time                    string       comment'支付时间',
    employee_id                     int          comment'负责意向的员工id',
    tdepart_id                      int          comment'销售部门id',
    tdepart_name                    string       comment'部门名称',
    class_id                        int          comment'班级id',
    itcast_school_id                string       comment'校区id',
    itcast_school_name              string       comment'校区名称',
    itcast_subject_id               string       comment'学科id',
    itcast_subject_name             string       comment'学科名称',
    `year`                          string       comment'年',
    `month`                         string       comment'年月',
    `day`                           string       comment'日'
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');
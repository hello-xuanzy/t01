--打卡明细表
insert into online_edu_dwb.dwb_signing_detail
select
    sig.id,
    time_table_id,
    sig.class_id,
    student_id,
    signin_time,
    signin_date,
    inner_flag,
    share_state,
    class_date,
    class_mode ,
    morning_begin_time,
    morning_end_time,
    afternoon_begin_time,
    afternoon_end_time,
    evening_begin_time,
    evening_end_time,
    use_begin_date,
    use_end_date
from online_edu_dwd.fact_tbh_student_signin_record sig
left join online_edu_dwd.dim_course_table_upload_detail cla
    on sig.class_id = cla.class_id
left outer join online_edu_dwd.dim_tbh_class_time_table ctime
    on sig.class_id = ctime.class_id
where signin_date = class_date and class_date between use_begin_date and use_end_date;

-- truncate table online_edu_dwb.dwb_signing_detail;
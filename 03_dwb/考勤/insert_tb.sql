select
    sr.id,
    sr.time_table_id,
    sr.class_id,
    sr.student_id,
    sr.signin_time,
    sr.signin_date,
    sr.inner_flag,
    sr.share_state,
    tt.morning_begin_time,
    tt.morning_end_time,
    tt.afternoon_begin_time,
    tt.afternoon_end_time,
    tt.evening_begin_time,
    tt.evening_end_time,
    tt.use_begin_date,
    tt.use_end_date,
    ud.class_date,
    ud.class_mode
from online_edu_dwd.fact_tbh_student_signin_record sr
left join online_edu_dwd.dim_tbh_class_time_table tt
on sr.class_id = tt.class_id
left join online_edu_dwd.dim_course_table_upload_detail ud
on sr.class_id = ud.class_id
where ud.class_date = sr.signin_date
  and sr.signin_time between use_begin_date and use_end_date;
-- 请假宽表
insert overwrite table online_edu_dwb.dwb_leave_detail
select
    la.id,
    la.class_id,
    la.student_id,
    la.audit_state,
    la.leave_type,
    substring(la.begin_time,1,19)as begin_time,
    la.begin_time_type,
    substring(la.end_time,1,19) as end_time,
    la.end_time_type,
    la.days,
    la.cancel_state,
    la.cancel_time,
    la.valid_state,
    tt.morning_begin_time,
    tt.morning_end_time,
    tt.afternoon_begin_time,
    tt.afternoon_end_time,
    tt.evening_begin_time,
    tt.evening_end_time,
    tt.use_begin_date,
    tt.use_end_date,
    ud.class_date,
    ud.class_mode
from online_edu_dwd.fact_student_leave_apply la
left join online_edu_dwd.dim_course_table_upload_detail ud
on la.class_id = ud.class_id
left join online_edu_dwd.dim_tbh_class_time_table tt
on ud.class_id = tt.class_id
where
    ud.class_date >= substring(la.begin_time,1,10)
    and ud.class_date <= substring(la.end_time,1,10)
    and ud.class_date >= tt.use_begin_date
    and ud.class_date <= tt.use_end_date;


-- dws_student_late(迟到)、dws_student_leave（请假）、dws_student_truancy（旷课）
-- 正常出勤统计表 dws_student_attendance
-- 统计每天每个班级各时段的正常出勤人数、出勤率
create table online_edu_dws.dws_student_attendance(
    class_id int comment '班级id',
    morning_count int comment '早上出勤人数',
    afternoon_count int comment '下午出勤人数',
    evening_count int comment '晚上出勤人数',
    morning_probability decimal(24,4) comment '早上出勤率',
    afternoon_probability decimal(24,4) comment '下午出勤率',
    evening_probability decimal(24,4) comment '晚上出勤率',
    date_day string comment '日期',
    studying_student_count int comment '学生人数'
)
comment '正常出勤表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');


create table online_edu_dws.dws_student_late(
    class_id int comment '班级id',
    morning_late_count int comment '早上迟到人数',
    afternoon_late_count int comment '下午迟到人数',
    evening_late_count int comment '晚上迟到人数',
    morning_late_probability decimal(24,4) comment '早上迟到率',
    afternoon_late_probability decimal(24,4) comment '下午迟到率',
    evening_late_probability decimal(24,4) comment '晚上迟到率',
    date_day string comment '日期',
    studying_student_count int comment '学生人数'
)
comment '学生迟到表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

create table online_edu_dws.dws_student_leave(
    class_id int comment '班级id',
    morning_leave_count int comment '早上请假人数',
    afternoon_leave_count int comment '下午请假人数',
    evening_leave_count int comment '晚上请假人数',
    morning_leave_probability decimal(24,4) comment '早上请假率',
    afternoon_leave_probability decimal(24,4) comment '下午请假率',
    evening_leave_probability decimal(24,4) comment '晚上请假率',
    date_day string comment '日期',
    studying_student_count int comment '班级学生总人数'
)
comment '学生迟到表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

create table online_edu_dws.dws_student_truancy(
    class_id int comment '班级id',
    morning_truancy_count int comment '早上请假人数',
    afternoon_truancy_count int comment '下午请假人数',
    evening_truancy_count int comment '晚上请假人数',
    morning_truancy_probability decimal(24,4) comment '早上请假率',
    afternoon_truancy_probability decimal(24,4) comment '下午请假率',
    evening_truancy_probability decimal(24,4) comment '晚上请假率',
    date_day string comment '日期',
    studying_student_count int comment '班级学生总人数'
)
comment '学生旷课表'
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');
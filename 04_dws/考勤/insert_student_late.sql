insert into online_edu_dws.dws_student_late
with a as
(select class_id,class_date,student_id,
       min(if(signin_time between
            cast(date_add('minute',-40,cast(concat(class_date,' ',morning_begin_time) as timestamp))as varchar)
            and concat(class_date,' ',morning_end_time),signin_time,null)) morning_signin_time,
       min(if(signin_time between
            cast(date_add('minute',-40,cast(concat(class_date,' ',afternoon_begin_time) as timestamp))as varchar)
            and concat(class_date,' ',afternoon_end_time),signin_time,null)) afternoon_signin_time,
       min(if(signin_time between
            cast(date_add('minute',-40,cast(concat(class_date,' ',evening_begin_time) as timestamp))as varchar)
            and concat(class_date,' ',evening_end_time),signin_time,null)) evening_signin_time
from online_edu_dwb.dwb_signing_detail
group by class_id,class_date,student_id),
b as (select
    a.class_id,
       --正常签到有效时间
       -- 开始时间前40min --> 开始时间
    cast(sum(if(signin_time between concat(a.class_date,' ',morning_begin_time)  and
        concat(a.class_date,' ',morning_end_time)
        ,1,0)) as integer) as morning_late_count,
    cast(sum(if(signin_time between  concat(a.class_date,' ',afternoon_begin_time)  and
        concat(a.class_date,' ',afternoon_end_time)
        ,1,0))as integer) afternoon_late_count,
    cast(sum(if(signin_time between concat(a.class_date,' ',evening_begin_time)  and
        concat(a.class_date,' ',evening_end_time)
        ,1,0))as integer) evening_late_count,
    a.class_date as date_day,studying_student_count,
    row_number() over(partition by a.class_id,a.class_date,studying_student_count,signin_time) as rn
from online_edu_dwb.dwb_signing_detail b join a
    on a.student_id = b.student_id and
       (signin_time =a.morning_signin_time
            or signin_time=a.afternoon_signin_time
            or signin_time=a.evening_signin_time)
    left join online_edu_ods.t_class_studying_student_count  c
        on a.class_id=c.class_id and a.class_date =c.studying_date
--共享屏幕状态 0 否 1是，在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0
-- where (share_state = 1 and inner_flag =0) or (share_state=1 and inner_flag=1)
where class_mode =0 or class_mode=1 or class_mode=2 or class_mode=3
group by a.class_id,a.class_date,studying_student_count,signin_time)
select
    class_id,
    cast(sum(morning_late_count)as integer) as morning_late_count,
    cast(sum(afternoon_late_count)as integer) as afternoon_late_count,
    cast(sum(evening_late_count) as integer)as evening_late_count,
    sum(morning_late_count)*1.0000/studying_student_count as morning_late_probability,
    sum(afternoon_late_count)*1.0000/studying_student_count as afternoon_late_probability,
    sum(evening_late_count)*1.0000/studying_student_count as evening_late_probability,
    date_day,
    studying_student_count
from b
where rn =1
group by class_id,date_day,studying_student_count;

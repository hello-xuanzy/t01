-- insert into online_edu_dws.dws_student_leave
--todo 请假人数  审核状态 2表示不通过 1、0表示通过
-- 请假开始时间 （begin_time） 结束时间 （end_time 1.这天是上课时间 2.开始时间属于那个时间段（上下晚）
-- 判断上午 begin_time<morning_end_time and end_time > morning_begin_time 则早上请假了
-- 判断下午 begin_time<afternoon_end_time and end_time > afternoon_begin_time 则下午请假了
-- 判断晚上 begin_time<evening_end_time and  end_time > evening_begin_time 则晚上请假了
-- 已请假天数 （days） 上课天(class_date)才有效   (dwb已做判断)
insert into online_edu_dws.dws_student_leave
select
       a.class_id,
       cast(sum(if(begin_time<concat(class_date,' ',morning_end_time) and end_time > concat(class_date,' ',morning_begin_time),1,0))as integer) morning_leave_count,
       cast(sum(if(begin_time<concat(class_date,' ',afternoon_end_time) and end_time > concat(class_date,' ',afternoon_begin_time),1,0))as integer)  afternoon_leave_count,
       cast(sum(if(begin_time<concat(class_date,' ',evening_end_time) and end_time > concat(class_date,' ',evening_begin_time),1,0))as integer)  evening_leave_count,
       cast(sum(if(begin_time<concat(class_date,' ',morning_end_time) and end_time > concat(class_date,' ',morning_begin_time),1,0)*1.0000/studying_student_count) as decimal(24,4)) morning_leave_probability,
       cast(sum(if(begin_time<concat(class_date,' ',afternoon_end_time) and end_time > concat(class_date,' ',afternoon_begin_time),1,0)*1.0000/studying_student_count)as decimal(24,4)) afternoon_leave_probability,
       cast(sum(if(begin_time<concat(class_date,' ',evening_end_time) and end_time > concat(class_date,' ',evening_begin_time),1,0)*1.0000/studying_student_count)as decimal(24,4)) evening_leave_probability,
       class_date as date_day,
       studying_student_count
from online_edu_dwb.dwb_leave_detail a left join online_edu_ods.t_class_studying_student_count b on a.class_id=b.class_id and a.class_date=b.studying_date
group by a.class_id,class_date,studying_student_count;

insert overwrite table online_edu_dws.dws_student_truancy
select
       coalesce (a.class_id,b.class_id,c.class_id) as class_id,
       ac.studying_student_count-coalesce (a.morning_count,0)-coalesce (b.morning_late_count,0)-coalesce (c.morning_leave_count,0) as morning_truancy_count,
       ac.studying_student_count-coalesce (a.afternoon_count,0)-coalesce (b.afternoon_late_count,0)-coalesce (c.afternoon_leave_count,0) as afternoon_truancy_count,
       ac.studying_student_count-coalesce (a.evening_count,0)-coalesce (b.evening_late_count,0)-coalesce (c.evening_leave_count,0) as evening_truancy_count,
       cast(1-coalesce (a.morning_probability,0)-coalesce (b.morning_late_probability,0)-coalesce (c.morning_leave_probability,0)as decimal(24,4)) as morning_truancy_probability,
       cast(1-coalesce (a.afternoon_probability,0)-coalesce (b.afternoon_late_probability,0)-coalesce (c.afternoon_leave_probability,0)as decimal(24,4)) as afternoon_truancy_probability,
       cast(1-coalesce (a.evening_probability,0)-coalesce (b.evening_late_probability,0)-coalesce (c.evening_leave_probability,0)as decimal(24,4)) as evening_truancy_probability,
       coalesce(a.date_day,b.date_day,c.date_day) as date_day,
       ac.studying_student_count
from online_edu_dws.dws_student_attendance a
    left join online_edu_ods.t_class_studying_student_count ac
        on a.class_id=ac.class_id and a.date_day=ac.studying_date
    full join online_edu_dws.dws_student_late b
    on a.class_id=b.class_id and a.date_day=b.date_day
    full join online_edu_dws.dws_student_leave c
    on a.class_id=c.class_id and a.date_day=c.date_day

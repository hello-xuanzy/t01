#!/bin/bash
hive -e "
create database if not exists online_edu_dws;
use online_edu_dws;
drop table if exists dws_signup_info_table;
create table dws_signup_info_table(
    time_type                       string       comment'时间维度类型',
    other_type                      string       comment'其他维度类型',
    relationship_cnt                bigint       comment'意向人数',
    appeal_cnt         	            bigint		 comment'有效线索人数',
    signup_cnt                      bigint       comment'报名人数',
    origin_type                     string       comment'线上线下',
    origin_channel                  string       comment'来源渠道',
    tdepart_id                      int          comment'销售部门id',
    tdepart_name                    string       comment'部门名称',
    itcast_school_id                string       comment'校区id',
    itcast_school_name              string       comment'校区名称',
    itcast_subject_id               string       comment'学科id',
    itcast_subject_name             string       comment'学科名称',
    `year`                          string       comment'年',
    `month`                         string       comment'年月',
    `day`                           string       comment'日'
)
row format delimited fields terminated by '\t'
stored as orc
tblproperties('orc.compress'='SNAPPY');
"
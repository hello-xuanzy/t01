#!/bin/bash
/export/server/presto/bin/presto --catalog hive --server hadoop01:8090 --execute "
--需求一
insert into mysql.online_edu_da.time_school_signup_cnt
select *
from hive.online_edu_da.time_school_signup_cnt;
--需求二
insert into mysql.online_edu_da.time_type_school_signup_cnt
select *
from hive.online_edu_da.time_type_school_signup_cnt;
--需求三
insert into mysql.online_edu_da.time_type_subject_signup_cnt
select *
from hive.online_edu_da.time_type_subject_signup_cnt;
--需求四
insert into mysql.online_edu_da.time_type_school_subject_signup_cnt
select *
from hive.online_edu_da.time_type_school_subject_signup_cnt;
--需求五
insert into mysql.online_edu_da.time_type_channel_signup_cnt
select *
from hive.online_edu_da.time_type_channel_signup_cnt;
--需求六
insert into mysql.online_edu_da.time_type_tdepart_signup_cnt
select *
from hive.online_edu_da.time_type_tdepart_signup_cnt;
--需求七
insert into mysql.online_edu_da.time_type_relationship_rate
select *
from hive.online_edu_da.time_type_relationship_rate;
--需求八
insert into mysql.online_edu_da.time_type_appeal_rate
select *
from hive.online_edu_da.time_type_relationship_rate;
"
insert into online_edu_dwd.dim_class_studying_student_count
select id,
    class_id,
    studying_student_count,
    studying_date
from online_edu_ods.t_class_studying_student_count;

insert into online_edu_dwd.dim_course_table_upload_detail
select id,
    base_id,
    class_id,
    class_date,
       --上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播
    class_mode
from online_edu_ods.t_course_table_upload_detail;

insert into online_edu_dwd.dim_tbh_class_time_table
select  id,
    class_id,
    morning_template_id,
    morning_begin_time,
    morning_end_time,
    afternoon_template_id,
    afternoon_begin_time,
    afternoon_end_time ,
    evening_template_id,
    evening_begin_time,
    evening_end_time,
    use_begin_date,
    use_end_date,
    create_time
from online_edu_ods.t_tbh_class_time_table;

insert into online_edu_dwd.fact_student_leave_apply
select id,
    class_id,
    student_id,
       --0 待审核 1 通过 2 不通过
    audit_state,
       -- 1 请假 2 销假'
    leave_type,
    begin_time,
       --1：上午 2：下午',
    begin_time_type,
    end_time,
       --1：上午 2：下午',
    end_time_type,
    days,
       --撤销状态  0 未撤销 1 已撤销
    cancel_state,
    cancel_time,
       --是否有效（0：无效 1：有效）
    valid_state,
    create_time
from online_edu_ods.t_student_leave_apply;

insert into online_edu_dwd.fact_tbh_student_signin_record
select id,
    time_table_id,
    class_id,
    student_id,
    substring(signin_time,1,19)as signin_time,
    signin_date,
       --0 外网 1 内网
    inner_flag ,
    share_state
from online_edu_ods.t_tbh_student_signin_record;

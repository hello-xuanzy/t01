-- --------------dim-------------------
--提取在读学生人数表(id、class_id、studying_student_count)
create table online_edu_dwd.dim_class_studying_student_count(
    id int comment 'id',
    class_id int comment '班级id',
    studying_student_count int comment '在读班级人数',
    studying_date   string comment '在读日期'
)
comment '在读学生人数'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

--班级课表
create table online_edu_dwd.dim_course_table_upload_detail(
    id int,
    base_id       int comment '课程主表id',
    class_id      int comment '班级id',
    class_date    string comment '上课日期',
    class_mode    int comment '上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播'
)
comment '班级课表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

--班级作息时间表
create table online_edu_dwd.dim_tbh_class_time_table(
    id                    int comment '主键id',
    class_id              int comment '班级id',
    morning_template_id   int comment '上午出勤模板id',
    morning_begin_time    string comment '上午开始时间',
    morning_end_time      string comment '上午结束时间',
    afternoon_template_id int comment '下午出勤模板id',
    afternoon_begin_time  string comment '下午开始时间',
    afternoon_end_time    string comment '下午结束时间',
    evening_template_id   int comment '晚上出勤模板id',
    evening_begin_time    string comment '晚上开始时间',
    evening_end_time      string comment '晚上结束时间',
    use_begin_date        string comment '使用开始日期',
    use_end_date          string comment '使用结束日期',
    create_time           string comment '创建时间'
)
comment '班级作息时间表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

--

-- --------------fact--------------------
create table online_edu_dwd.fact_student_leave_apply(
    id              int,
    class_id        int comment '班级id',
    student_id      int comment '学员id',
    audit_state     int comment '审核状态 0 待审核 1 通过 2 不通过',
    leave_type      int comment '请假类型  1 请假 2 销假',
    begin_time      string comment '请假开始时间',
    begin_time_type int comment '1：上午 2：下午',
    end_time        string comment '请假结束时间',
    end_time_type   int comment '1：上午 2：下午',
    days            float comment '请假/已休天数',
    cancel_state    string comment '撤销状态  0 未撤销 1 已撤销',
    cancel_time     string comment '撤销时间',
    valid_state     int comment '是否有效（0：无效 1：有效）',
    create_time     string comment '创建时间'
)
comment '请假申请表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');

--学生打卡记录表
create table online_edu_dwd.fact_tbh_student_signin_record(
    id                int comment '主键id',
    time_table_id     int comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    class_id          int comment '班级id',
    student_id        int comment '学员id',
    signin_time       string comment '签到时间',
    signin_date       string comment '签到日期',
    inner_flag        int comment '内外网标志  0 外网 1 内网',
    share_state       int comment '共享屏幕状态 0 否 1是  在上午或下午段有共屏记录，则该段所有记录该字段为1，内网默认为1 外网默认为0 '
)
comment '学生打卡记录表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='SNAPPY');